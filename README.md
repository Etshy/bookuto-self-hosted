## **INSTALLATION**

Install PHP and extensions :
`apt install php8.0 php8.0-amqp php8.0-bcmath php8.0-bz2 php8.0-cli php8.0-common php8.0-curl php8.0-dev php8.0-fpm php8.0-gd php8.0-gmp php8.0-imagick php8.0-intl php8.0-ldap php8.0-mbstring php8.0-mongodb php8.0-mysql php8.0-odbc php8.0-opcache php8.0-readline php8.0-sqlite3 php8.0-xml php8.0-zip`

Configure your `php.ini`  like you want (like increasing post upload size, etc.)

Strongly recommended to NOT do the following while being logged in as root. Be careful with files permissions, the best is to `chown` the folder and all its file to the server user (eg: www-data:www-data for apache)

Download the reader and extract it in the folder you want

Move to the reader folder and install dependencies

`composer install --no-dev --optimize-autoloader` to install PHP app dependencies

Front end, and especially fomantic, is a MESS !

To simplify installation I made a coommand that does everyting : 
`php bin/console bookuto:install-frontend`

This will install and build the frontend assets.

You can then access the app in your browser. If everything is right with your server config, it should redirect to the `/install` page.

On this page you have to fill all fields to completely install the app.

## AMQP is not necessary!!

AMQP can be used to improve direct performances in some actions by delaying some processes.

If you want to use it, you have to install it, configure it and change parameters in `config/packages/messenger.yaml`

And then configure an async Message consumer (like using supervisor as async process that consume message : https://symfony.com/doc/current/messenger.html#messenger-worker)

## Thanks

