<?php
declare(strict_types=1);

namespace App\Message;

use App\Message\DTO\ChapterDTO;
use App\Message\Interfaces\NotificationInterface;

/**
 * Class ChapterNotificationMessage
 * @package App\Message
 */
class ChapterNotificationMessage implements NotificationInterface
{
    protected ChapterDTO $chapter;

    public function getChapter(): ChapterDTO
    {
        return $this->chapter;
    }

    public function setChapter(ChapterDTO $chapter): void
    {
        $this->chapter = $chapter;
    }
}
