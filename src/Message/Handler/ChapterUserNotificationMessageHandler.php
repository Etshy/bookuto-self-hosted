<?php
declare(strict_types=1);

namespace App\Message\Handler;

use App\Exceptions\ChapterDTOToModelException;
use App\Exceptions\UserDTOToModelException;
use App\Mailer\Mail;
use App\Mailer\Mailer;
use App\Message\ChapterUserNotificationMessage;
use App\Message\EmailMessage;
use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\ChapterNotificationInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Model\UserSettingsInterface;
use App\Service\ChapterNotificationService;
use App\Service\ChapterService;
use App\Service\UserService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * Class ChapterUserNotificationMessageHandler
 * @package App\Message\Handler
 */
class ChapterUserNotificationMessageHandler implements MessageHandlerInterface
{
    protected TranslatorInterface $translator;
    private UserService $userService;
    private ChapterService $chapterService;
    private ChapterNotificationService $chapterNotificationService;
    private Mailer $mailer;
    private MessageBusInterface $bus;

    public function __construct(
        UserService $userService,
        ChapterService $chapterService,
        ChapterNotificationService $chapterNotificationService,
        Mailer $mailer,
        MessageBusInterface $bus,
        TranslatorInterface $translator
    ) {
        $this->userService = $userService;
        $this->chapterService = $chapterService;
        $this->chapterNotificationService = $chapterNotificationService;
        $this->mailer = $mailer;
        $this->bus = $bus;
        $this->translator = $translator;
    }

    public function __invoke(ChapterUserNotificationMessage $message): bool
    {
        try {
            $userDTO = $message->getUser();
            $chapterDTO = $message->getChapter();
            $user = $this->userService->find($userDTO->getId());
            if (!$user instanceof UserInterface) {
                throw new UserDTOToModelException();
            }
            $chapter = $this->chapterService->find($chapterDTO->getId());
            if (!$chapter instanceof ChapterInterface) {
                throw new ChapterDTOToModelException();
            }
            $criteria = [
                'user' => $user,
                'series' => $chapter->getSeries(),
                'readen' => false,
            ];
            $chapterNotificationAlreadyExists = $this->chapterNotificationService->findOneByCriteria($criteria);
            if ($chapterNotificationAlreadyExists instanceof ChapterNotificationInterface) {
                //A chapterNotification for this series already exists and is not readen
                return false;
            }
            $chapterNotification = $this->chapterNotificationService->createObject();
            $chapterNotification->setChapter($chapter);
            $chapterNotification->setSeries($chapter->getSeries());
            $chapterNotification->setUser($user);
            $this->chapterNotificationService->save($chapterNotification);
            if ($user->getSettings() instanceof UserSettingsInterface
                && $user->getSettings()->isChapterEmailNotificationActivated()
            ) {
                $mail = new Mail();
                $mail->setToEmail([$user->getEmail()]);
                $mail->setSubject($this->translator->trans('chapter.released'));
                $mail = $this->mailer->renderTemplateMail($mail, 'mail/chapter_notification.html.twig', [
                    'chapter' => $chapter,
                ]);
                $emailMessage = new EmailMessage();
                $emailMessage->setMail($mail);
                $this->bus->dispatch($emailMessage);
            }
        } catch (ChapterDTOToModelException | UserDTOToModelException | OptimisticLockException | ORMException | Throwable $e) {

        }

        return true;
    }
}
