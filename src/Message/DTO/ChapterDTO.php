<?php
declare(strict_types=1);

namespace App\Message\DTO;

use App\Message\DTO\Interfaces\ChapterDTOInterface;
use DateTime;

/**
 * Class ChapterDTOTransformer
 * A "copy" of the Chapter Entity/Document to be serialized in message.
 * @package App\Message\DTO
 */
class ChapterDTO implements ChapterDTOInterface
{
    protected string|int $id;

    protected ?DateTime $createdAt;

    protected ?DateTime $updatedAt;

    protected ?string $name;

    protected ?float $number;

    protected ?bool $visible;

    protected ?string $language;

    protected ?SeriesDTO $series;

    protected ?DateTime $publishedAt;

    public function getId(): int|string
    {
        return $this->id;
    }

    public function setId(int|string $id): void
    {
        $this->id = $id;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getNumber(): ?float
    {
        return $this->number;
    }

    public function setNumber(?float $number): void
    {
        $this->number = $number;
    }

    public function isVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): void
    {
        $this->visible = $visible;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    public function getSeries(): ?SeriesDTO
    {
        return $this->series;
    }

    public function setSeries(?SeriesDTO $series): void
    {
        $this->series = $series;
    }

    public function getPublishedAt(): ?DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }
}
