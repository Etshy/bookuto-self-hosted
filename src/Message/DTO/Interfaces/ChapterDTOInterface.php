<?php
declare(strict_types=1);

namespace App\Message\DTO\Interfaces;

use App\Message\DTO\SeriesDTO;
use DateTime;

/**
 * Interface ChapterDTOInterface
 * @package App\Message\DTO\Interfaces
 */
interface ChapterDTOInterface extends DTOInterface
{
    public function getCreatedAt(): ?DateTime;

    public function setCreatedAt(?DateTime $createdAt): void;

    public function getUpdatedAt(): ?DateTime;

    public function setUpdatedAt(?DateTime $updatedAt): void;

    public function getName(): ?string;

    public function setName(?string $name): void;

    public function getNumber(): ?float;

    public function setNumber(?float $number): void;

    public function isVisible(): ?bool;

    public function setVisible(?bool $visible): void;

    public function getLanguage(): ?string;

    public function setLanguage(?string $language): void;

    public function getSeries(): ?SeriesDTO;

    public function setSeries(?SeriesDTO $series): void;

    public function getPublishedAt(): ?DateTime;

    public function setPublishedAt(?DateTime $publishedAt): void;
}
