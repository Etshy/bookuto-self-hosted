<?php
declare(strict_types=1);

namespace App\Message;

use App\Mailer\Mail;

/**
 * Class EmailMessage
 * @package App\Message
 */
class EmailMessage
{

    private Mail $mail;

    public function getMail(): Mail
    {
        return $this->mail;
    }

    public function setMail(Mail $mail): void
    {
        $this->mail = $mail;
    }
}
