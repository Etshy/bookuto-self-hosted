<?php
declare(strict_types=1);

namespace App;

use App\DependencyInjection\CompilerPass\DoctrineRepositoryCompilerPass;
use App\Security\ResetPassword\DependencyInjection\ResetPasswordCompilerPass;
use App\Security\ResetPassword\DependencyInjection\ResetPasswordExtension;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Doctrine\Bundle\MongoDBBundle\DependencyInjection\Compiler\DoctrineMongoDBMappingsPass;
use Exception;
use Generator;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use function dirname;

/**
 * Class Kernel
 * @package App
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function registerBundles(): iterable|Generator
    {
        date_default_timezone_set("UTC");

        $contents = require $this->getProjectDir().'/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new DoctrineRepositoryCompilerPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 10);

        $mappingsODM = [
            realpath(__DIR__.'/Model/Mapping/ODM') => 'App\Model\Persistence',
            realpath(__DIR__.'/Model/Mapping/ODM/Embed') => 'App\Model\Persistence\Embed',
            realpath(__DIR__.'/Model/Mapping/ODM/Files') => 'App\Model\Persistence\Files',
        ];
        $mappingsORM = [
            realpath(__DIR__.'/Model/Mapping/ORM') => 'App\Model\Persistence',
            realpath(__DIR__.'/Model/Mapping/ORM/Embed') => 'App\Model\Persistence\Embed',
            realpath(__DIR__.'/Model/Mapping/ORM/Files') => 'App\Model\Persistence\Files',
        ];

        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(DoctrineOrmMappingsPass::createXmlMappingDriver($mappingsORM, [], 'app.backend_type_orm'));
        }

        if (class_exists(DoctrineMongoDBMappingsPass::class)) {
            $container->addCompilerPass(DoctrineMongoDBMappingsPass::createXmlMappingDriver($mappingsODM, [], 'app.backend_type_mongodb'));
        }


    }

    /**
     * @param ContainerBuilder $container
     * @param LoaderInterface $loader
     *
     * @throws Exception
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
        $container->registerExtension(new ResetPasswordExtension());
        $container->loadFromExtension('reset_password');

        $container->setParameter('container.autowiring.strict_mode', true);
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir().'/config';
        $loader->load($confDir.'/packages/*'.self::CONFIG_EXTS, 'glob');
        if (is_dir($confDir.'/packages/'.$this->environment)) {
            $loader->load($confDir.'/packages/'.$this->environment.'/**/*'.self::CONFIG_EXTS, 'glob');
        }
        $loader->load($confDir.'/services'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/services_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/'.$this->environment.'/*.yaml');
        $routes->import('../config/{routes}/*.yaml');

        if (is_file(dirname(__DIR__).'/config/routes.yaml')) {
            $routes->import('../config/{routes}.yaml');
        } elseif (is_file($path = dirname(__DIR__).'/config/routes.php')) {
            (require $path)($routes->withPath($path), $this);
        }
    }

}
