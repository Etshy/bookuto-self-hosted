<?php
declare(strict_types=1);


namespace App\Application\Series;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class StatusManager
 * @package App\Application\Series
 */
class StatusManager
{
    public const STATUS_ONGOING = 'ongoing';
    public const STATUS_ON_HOLD = 'onhold';
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUT_COMPLETED = 'completed';

    protected TranslatorInterface $translator;

    /**
     * StatusManager constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        return [
            self::STATUS_ONGOING,
            self::STATUS_ON_HOLD,
            self::STATUS_CANCELLED,
            self::STATUT_COMPLETED,
        ];
    }

    /**
     * @return string[]
     */
    public function getStatusForForm(): array
    {
        return [
            $this->translator->trans('label.ongoing') => self::STATUS_ONGOING,
            $this->translator->trans('label.onhold') => self::STATUS_ON_HOLD,
            $this->translator->trans('label.cancelled') => self::STATUS_CANCELLED,
            $this->translator->trans('label.completed') => self::STATUT_COMPLETED,
        ];
    }
}
