<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\ChapterReadenInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\ChapterReadenRepositoryInterface;
use Doctrine\ORM\Exception\ORMException;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Security\Core\Security;

/**
 * Class ChapterReadenService
 * @package App\Service
 */
class ChapterReadenService extends BaseModelService
{
    /**
     * @var Security
     */
    private Security $security;

    /**
     * ChapterReadenService constructor.
     *
     * @param ChapterReadenRepositoryInterface $chapterReadRepository
     * @param Security $security
     */
    public function __construct(ChapterReadenRepositoryInterface $chapterReadRepository, Security $security)
    {
        $this->repository = $chapterReadRepository;
        $this->om = $chapterReadRepository->getObjectManager();
        $this->security = $security;
    }

    /**
     * @return ChapterReadenInterface[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param SeriesInterface $series
     * @param ChapterInterface $chapter
     *
     * @return bool
     * @throws ORMException|\Doctrine\ORM\ORMException
     */
    public function createChapterReadFor(SeriesInterface $series, ChapterInterface $chapter): bool
    {
        $user = $this->security->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        $existingChapterRead = $this->repository->findOneBy([
            'series.id' => $series->getId(),
            'chapter.id' => $chapter->getId(),
            'user.id' => $user->getId(),
        ]);

        if ($existingChapterRead instanceof ChapterReadenInterface) {
            return false;
        }

        $chapterRead = $this->createObject();
        $chapterRead->setSeries($series);
        $chapterRead->setChapter($chapter);
        $chapterRead->setUser($user);
        $this->save($chapterRead);

        return true;
    }

    public function createObject(): ChapterReadenInterface
    {
        $object = $this->repository->getClassName();

        return new $object();
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function getPagination(array $criteria, int $page = 1): Pagerfanta
    {
        return $this->repository->getPagination($criteria, $page);
    }

    /**
     * @param SeriesInterface $series
     * @param UserInterface|null $user
     *
     * @return array
     */
    public function getArrayChapterIdsBySeries(SeriesInterface $series, UserInterface $user = null): array
    {
        if (is_null($user)) {
            /** @noinspection CallableParameterUseCaseInTypeContextInspection */
            $user = $this->security->getUser();
            if (!$user instanceof UserInterface) {
                return [];
            }
        }

        $chaptersReaden = $this->findBy([
            'series.id' => $series->getId(),
            'user.id' => $user->getId(),
        ]);
        $ids = [];
        foreach ($chaptersReaden as $chapterReaden) {
            if (!$chapterReaden instanceof ChapterReadenInterface) {
                continue;
            }
            $ids[] = $chapterReaden->getChapter()->getId();
        }

        return $ids;
    }

    /**
     * @param array $criteria
     *
     * @return ChapterReadenInterface[]
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return ChapterReadenInterface
     */
    public function findOneBy(array $criteria): ChapterReadenInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $id
     *
     * @return ChapterReadenInterface
     */
    public function find(string $id): ChapterReadenInterface
    {
        return $this->repository->find($id);
    }
}
