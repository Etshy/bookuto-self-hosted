<?php declare(strict_types=1);

namespace App\Service;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Repository\FileRepositoryInterface;
use App\Model\Interfaces\Repository\ImageRepositoryInterface;
use App\Model\Persistence\Files\File;
use App\Model\Persistence\Files\Image;
use App\Model\Persistence\Files\ImageMetadata;
use App\Utils\FileManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileService
 * @package App\Service
 */
class FileService
{

    /**x
     * @var FileRepositoryInterface
     */
    protected FileRepositoryInterface $repository;
    /**
     * @var FileManager
     */
    protected FileManager $fileManager;
    /**
     * @var ImageRepositoryInterface
     */
    protected ImageRepositoryInterface $imageRepository;

    /**
     * FileService constructor.
     *
     * @param FileRepositoryInterface $fileRepository
     * @param ImageRepositoryInterface $imageRepository
     * @param FileManager $fileManager
     */
    public function __construct(FileRepositoryInterface $fileRepository, ImageRepositoryInterface $imageRepository, FileManager $fileManager)
    {
        $this->repository = $fileRepository;
        $this->fileManager = $fileManager;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @param BaseModelInterface $document
     *
     */
    public function remove(BaseModelInterface $document): void
    {
        $this->getRepository($document)->getObjectManager()->remove($document);
        $this->getRepository($document)->getObjectManager()->flush();
    }

    /**
     * @param array $ids
     *
     */
    public function batchRemove(array $ids): void
    {
        $this->repository->batchRemove($ids);
    }

    /**
     * @param string $id
     *
     * @return File|FileInterface|null
     */
    public function find(string $id): File|FileInterface|null
    {
        /** @var File $file */
        $file = $this->repository->find($id);
        if (!$file instanceof FileInterface) {
            return null;
        }

        if ($file->getMetadata() instanceof ImageMetadata) {
            $image = new Image();
            $data = $file->toArray();
            $image->hydrate($data);
            $file = $image;
        }

        return $file;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return File
     */
    public function findOneBy(array $criteria): File
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Save the image in the Database
     *
     * @throws FileNotFoundException
     */
    public function save(FileInterface $file): FileInterface
    {
        $fileDB = $this->getRepository($file)->save($file);
        if (!$fileDB instanceof FileInterface) {
            throw new FileNotFoundException();
        }

        return $fileDB;
    }

    /**
     * @param File $file
     */
    public function downloadToTmpFile(File $file): void
    {
        $realPath = '/tmp/'.uniqid('', true);
        $file->setRealPath($realPath);
        $this->getRepository($file)->setFileInFS($file);
    }

    /**
     * @param $fileId
     *
     * @return resource
     */
    public function getFileStream($fileId)
    {
        return $this->repository->getFileStream($fileId);
    }

    /**
     * @param UploadedFile|null $value
     *
     * @return FileInterface|null
     */
    public function createFileFromUploadedFile(UploadedFile $value = null): ?FileInterface
    {
        if (!$value instanceof UploadedFile) {
            return null;
        }

        $mime = $value->getMimeType();

        if (str_contains($mime, 'image')) {
            $file = new Image();
        } else {
            $file = new File();
        }

        $metadata = $this->fileManager->generateMetadataFromUploadedFile($value);
        $file->setMetadata($metadata);

        $file->setName($value->getClientOriginalName());
        $file->setRealPath($value->getRealPath());

        return $file;
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     */
    public function moveFile(UploadedFile $file, string $path): void
    {
        $file->move($path);
    }

    /**
     * @param $document
     *
     * @return FileRepositoryInterface
     */
    protected function getRepository($document): FileRepositoryInterface
    {
        return match (true) {
            $document instanceof ImageInterface => $this->imageRepository,
            default => $this->repository,
        };
    }
}
