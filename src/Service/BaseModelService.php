<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Repository\RepositoryInterface;
use App\Service\Interfaces\BaseModelServiceInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectManager;
use Exception;

/**
 * Class BaseService
 * Basic methods on all Model related Services
 * @package App\Service
 */
abstract class BaseModelService implements BaseModelServiceInterface
{
    /**
     * @var RepositoryInterface
     */
    protected RepositoryInterface $repository;

    /**
     * @var DocumentManager|EntityManager|ObjectManager
     */
    protected DocumentManager|EntityManager|ObjectManager $om;


    /**
     * @return RepositoryInterface
     */
    public function getRepository(): RepositoryInterface
    {
        return $this->repository;
    }

    /**
     * @return ObjectManager|DocumentManager|EntityManager
     */
    public function getOm(): EntityManager|ObjectManager|DocumentManager
    {
        return $this->om;
    }

    /**
     * @param array $ids
     *
     */
    public function batchRemove(array $ids): void
    {
        $this->repository->batchRemove($ids);
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function save(BaseModelInterface $object): void
    {
        if (!$object->getId()) {
            $this->om->persist($object);
        }
        $this->om->flush();
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws MongoDBException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(BaseModelInterface $object): void
    {
        $this->om->remove($object);
        $this->om->flush();
    }

    /**
     *
     * @return BaseModelInterface
     */
    public function createObject(): BaseModelInterface
    {
        $object = $this->repository->getClassName();

        return new $object();
    }

    /**
     * @return BaseModelInterface[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return BaseModelInterface[]
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }
}
