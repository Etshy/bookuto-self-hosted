<?php
declare(strict_types=1);


namespace App\Service;

use App\Exceptions\Services\Notification\UnsupportedNotificationTypeException;
use App\Model\Interfaces\Model\NotificationInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\NotificationRepositoryInterface;
use App\Model\Interfaces\Repository\RepositoryInterface;
use App\Model\Persistence\User;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Iterator;
use Pagerfanta\Pagerfanta;

/**
 * Class NotificationService
 * @package App\Service
 */
class NotificationService extends BaseModelService
{
    public const TYPE_CHAPTER = 'chapter';

    /**
     * NotificationService constructor.
     *
     * @param NotificationRepositoryInterface $repository
     */
    public function __construct(NotificationRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->om = $repository->getObjectManager();
    }

    /**
     * @return NotificationInterface
     * @throws UnsupportedNotificationTypeException
     */
    public function createObject(): NotificationInterface
    {
        throw new UnsupportedNotificationTypeException();
    }

    /**
     * @param array $criteria
     *
     * @return NotificationInterface
     */
    public function findOneBy(array $criteria): NotificationInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $id
     *
     * @return NotificationInterface
     */
    public function find(string $id): NotificationInterface
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return NotificationInterface|null
     */
    public function findOneByCriteria(array $criteria): ?NotificationInterface
    {
        return $this->repository->findOneByCriteria($criteria);
    }

    /**
     * @param User $user
     *
     * @return Iterator
     */
    public function findLatestNotifications(User $user): Iterator
    {
        $criteria = [
            'user' => $user,
            'limit' => 10,
            'orderBy' => [
                'fieldname' => 'createdAt',
                'order' => RepositoryInterface::SORT_DESC,
            ],
        ];

        return $this->findByCriteria($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return Iterator
     */
    public function findByCriteria(array $criteria): Iterator
    {
        return $this->repository->findByCriteria($criteria);
    }

    /**
     * @param UserInterface $user
     *
     * @return int
     */
    public function countUnread(UserInterface $user): int
    {
        $criteria = [
            'user' => $user,
            'readen' => false,
        ];

        return $this->repository->countUnread($criteria);
    }

    /**
     * @param UserInterface $user
     */
    public function setAllAsSentForUser(UserInterface $user): void
    {
        $this->repository->setAllAsSentForUser($user);
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function getPagination(array $criteria, int $page = 1): Pagerfanta
    {
        return $this->repository->getPagination($criteria, $page);
    }

    /**
     * @param NotificationInterface $notification
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setAsReaden(NotificationInterface $notification): void
    {
        $notification->setReaden(true);
        $notification->setSent(true);
        $this->save($notification);
    }

    /**
     * @param $user
     *
     * @return Iterator
     */
    public function getUnreadUnsentNotificationsForUser($user): Iterator
    {
        $criteria = [
            "user" => $user,
            "sent" => false,
            "readen" => false,
            'orderBy' => [
                'fieldname' => 'createdAt',
                'order' => RepositoryInterface::SORT_DESC,
            ],
        ];

        return $this->findByCriteria($criteria);
    }
}
