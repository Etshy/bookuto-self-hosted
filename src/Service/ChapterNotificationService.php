<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Interfaces\Model\ChapterNotificationInterface;
use App\Model\Interfaces\Repository\ChapterNotificationRepositoryInterface;
use App\Model\Persistence\ChapterNotification;
use JetBrains\PhpStorm\Pure;

/**
 * Class ChapterNotificationService
 * @package App\Service
 */
class ChapterNotificationService extends NotificationService
{
    /**
     * NotificationService constructor.
     *
     * @param ChapterNotificationRepositoryInterface $repository
     */
    public function __construct(ChapterNotificationRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @return ChapterNotificationInterface
     */
    #[Pure]
    public function createObject(): ChapterNotificationInterface
    {
        return new ChapterNotification();
    }
}
