<?php
declare(strict_types=1);

namespace App\Service;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Repository\TeamRepositoryInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Pagerfanta\Pagerfanta;

/**
 * Class TeamService
 * @package App\Service
 */
class TeamService extends BaseModelService
{
    /**
     * @var FileService
     */
    protected FileService $fileService;

    /**
     * TeamService constructor.
     *
     * @param TeamRepositoryInterface $teamRepository
     * @param FileService $fileService
     */
    public function __construct(TeamRepositoryInterface $teamRepository, FileService $fileService)
    {
        $this->repository = $teamRepository;
        $this->om = $teamRepository->getObjectManager();
        $this->fileService = $fileService;
    }

    /**
     *
     * @return TeamInterface
     */
    public function createObject(): TeamInterface
    {
        $object = $this->repository->getClassName();

        return $$object();
    }


    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return TeamInterface
     */
    public function findOneBy(array $criteria): TeamInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $id
     *
     * @return TeamInterface
     */
    public function find(string $id): TeamInterface
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findByCriteriaPaginated(array $criteria = [], int $page = 1): Pagerfanta
    {
        return $this->repository->findByCriteriaPaginated($criteria, $page);
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(BaseModelInterface $object): void
    {
        if ($object->getImage() instanceof ImageInterface && !is_null($object->getImage()->getRealPath())) {
            $file = $this->fileService->save($object->getImage());
            $object->setImage($file);
        }
        parent::save($object);
    }
}
