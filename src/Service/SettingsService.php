<?php
declare(strict_types=1);

namespace App\Service;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Repository\SettingsRepositoryInterface;
use App\Model\Persistence\Settings;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class SettingsService
 * @package App\Service
 */
class SettingsService extends BaseModelService
{
    /**
     * @var FileService
     */
    protected FileService $fileService;

    /**
     * SeriesService constructor.
     *
     * @param SettingsRepositoryInterface $settingsRepository
     * @param FileService $fileService
     */
    public function __construct(SettingsRepositoryInterface $settingsRepository, FileService $fileService)
    {
        $this->repository = $settingsRepository;
        $this->om = $settingsRepository->getObjectManager();
        $this->fileService = $fileService;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findBy(array $criteria): array
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return Settings
     */
    public function findOneBy(array $criteria): Settings
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $id
     *
     * @return Settings|null
     */
    public function find(string $id): ?Settings
    {
        return $this->repository->find($id);
    }

    /**
     * @return Settings
     */
    public function findSettings(): Settings
    {
        $result = $this->repository->findAll();
        if (is_array($result) && count($result) > 0) {
            $settings = reset($result);
        } else {
            $settings = $this->createObject();
        }

        return $settings;
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(BaseModelInterface $object): void
    {
        if ($object->getLogo() instanceof ImageInterface && !is_null($object->getLogo()->getRealPath())) {
            $file = $this->fileService->save($object->getLogo());
            $object->setLogo($file);
        }

        parent::save($object);
    }
}
