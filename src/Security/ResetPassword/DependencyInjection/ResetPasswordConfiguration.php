<?php declare(strict_types=1);

namespace App\Security\ResetPassword\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 *
 */
class ResetPasswordConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('reset_password');
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->integerNode('lifetime')
            ->defaultValue(3600)
            ->info('The length of time in seconds that a password reset request is valid for after it is created.')
            ->end()
            ?->integerNode('throttle_limit')
            ->defaultValue(3600)
            ->info('Another password reset cannot be made faster than this throttle time in seconds.')
            ->end();

        return $treeBuilder;
    }
}
