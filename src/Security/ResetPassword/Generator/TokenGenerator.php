<?php declare(strict_types=1);

namespace App\Security\ResetPassword\Generator;

use App\Security\ResetPassword\Model\ResetPasswordTokenComponent;
use App\Security\ResetPassword\Utils\ResetPasswordHelper;
use DateTimeInterface;
use Exception;

/**
 *
 */
class TokenGenerator
{
    protected string $signingKey;
    protected TokenGenerator $generator;

    public function __construct(string $signingKey)
    {
        $this->signingKey = $signingKey;
    }

    /**
     * @throws Exception
     */
    public function createToken(DateTimeInterface $expiresAt, $userId, string $verifier = null): ResetPasswordTokenComponent
    {
        if (null === $verifier) {
            $verifier = $this->getRandomAlphaNumStr();
        }

        $selector = $this->getRandomAlphaNumStr();

        $jsonData = json_encode([$verifier, $userId, $expiresAt->getTimestamp()], JSON_THROW_ON_ERROR);

        return new ResetPasswordTokenComponent(
            $selector,
            $verifier,
            $this->getHashedToken($jsonData)
        );
    }

    private function getHashedToken(string $data): string
    {
        return base64_encode(hash_hmac('sha256', $data, $this->signingKey, true));
    }

    /**
     * @throws Exception
     */
    private function getRandomAlphaNumStr(): string
    {
        $string = '';

        while (($len = strlen($string)) < ResetPasswordHelper::SELECTOR_LENGTH) {
            $size = ResetPasswordHelper::SELECTOR_LENGTH - $len;

            $bytes = random_bytes($size);

            $string .= substr(
                str_replace(['/', '+', '='], '', base64_encode($bytes)),
                0,
                $size
            );
        }

        return $string;
    }
}
