<?php
declare(strict_types=1);

namespace App\Security;

use App\Model\Persistence\User;
use App\Voter\ChapterVoter;
use App\Voter\SeriesVoter;
use App\Voter\SettingsVoter;
use App\Voter\TeamVoter;
use App\Voter\UserVoter;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RoleService
 * @package App\Service
 */
class RoleService
{

    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * RoleService constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Return an array with all the value
     * @return array
     */
    public function getAllRoles(): array
    {
        $roles = [];
        $roles += $this->getChapterRoles();
        $roles += $this->getSeriesRoles();
        $roles += $this->getSettingsRoles();
        $roles += $this->getTeamRoles();
        $roles += $this->getUserRoles();

        return $roles;
    }

    /**
     * @return array
     */
    public function getChapterRoles(): array
    {
        return [
            $this->translator->trans('roles.chapter.list') => ChapterVoter::ROLE_LIST_CHAPTER,
            $this->translator->trans('roles.chapter.view') => ChapterVoter::ROLE_VIEW_CHAPTER,
            $this->translator->trans('roles.chapter.add') => ChapterVoter::ROLE_ADD_CHAPTER,
            $this->translator->trans('roles.chapter.edit') => ChapterVoter::ROLE_EDIT_CHAPTER,
            $this->translator->trans('roles.chapter.delete') => ChapterVoter::ROLE_DELETE_CHAPTER,
        ];
    }

    /**
     * @return array
     */
    public function getSeriesRoles(): array
    {
        return [
            $this->translator->trans('roles.series.list') => SeriesVoter::ROLE_LIST_SERIES,
            $this->translator->trans('roles.series.view') => SeriesVoter::ROLE_VIEW_SERIES,
            $this->translator->trans('roles.series.add') => SeriesVoter::ROLE_ADD_SERIES,
            $this->translator->trans('roles.series.edit') => SeriesVoter::ROLE_EDIT_SERIES,
            $this->translator->trans('roles.series.delete') => SeriesVoter::ROLE_DELETE_SERIES,
        ];
    }

    /**
     * @return array
     */
    public function getSettingsRoles(): array
    {
        return [
            $this->translator->trans('roles.settings.general') => SettingsVoter::ROLE_EDIT_GENERAL_SETTINGS,
            $this->translator->trans('roles.settings.reader') => SettingsVoter::ROLE_EDIT_READER_SETTINGS,
        ];
    }

    /**
     * @return array
     */
    public function getTeamRoles(): array
    {
        return [
            $this->translator->trans('roles.team.list') => TeamVoter::ROLE_LIST_TEAMS,
            $this->translator->trans('roles.team.view') => TeamVoter::ROLE_VIEW_TEAM,
            $this->translator->trans('roles.team.add') => TeamVoter::ROLE_ADD_TEAM,
            $this->translator->trans('roles.team.edit') => TeamVoter::ROLE_EDIT_TEAM,
            $this->translator->trans('roles.team.delete') => TeamVoter::ROLE_DELETE_TEAM,
        ];
    }

    /**
     * @return array
     */
    public function getUserRoles(): array
    {
        return [
            $this->translator->trans('roles.user.super-admin') => User::ROLE_SUPER_ADMIN,
            $this->translator->trans('roles.user.admin') => UserVoter::ROLE_ADMIN,
            $this->translator->trans('roles.user.list') => UserVoter::ROLE_LIST_USERS,
            $this->translator->trans('roles.user.view') => UserVoter::ROLE_VIEW_USER,
            $this->translator->trans('roles.user.add') => UserVoter::ROLE_ADD_USER,
            $this->translator->trans('roles.user.edit') => UserVoter::ROLE_EDIT_USER,
            $this->translator->trans('roles.user.edit-roles') => UserVoter::ROLE_EDIT_USER_ROLE,
            $this->translator->trans('roles.user.delete') => UserVoter::ROLE_DELETE_USER,
        ];
    }

    /**
     * Return a formatted array for Form (Roles Grouped)
     * @return array
     */
    public function getAllRolesForForm(): array
    {
        return [
            $this->translator->trans('roles.chapter.form.label') => $this->getChapterRoles(),
            $this->translator->trans('roles.series.form.label') => $this->getSeriesRoles(),
            $this->translator->trans('roles.settings.form.label') => $this->getSettingsRoles(),
            $this->translator->trans('roles.team.form.label') => $this->getTeamRoles(),
            $this->translator->trans('roles.user.form.label') => $this->getUserRoles(),
        ];
    }
}
