<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model;

use DateTime;

/**
 * Interface SoftDeleteable
 * @package App\Model\Interfaces\Model
 */
interface SoftDeleteable
{
    public function setDeletedAt(DateTime $deletedAt = null): void;

    public function getDeletedAt(): ?DateTime;

    public function isDeleted(): bool;
}
