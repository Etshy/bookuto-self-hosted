<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface SluggableInterface
 * @package App\Model\Interfaces\Model
 */
interface SluggableInterface
{
    public function getName(): string;

    public function setName(string $name): void;

}
