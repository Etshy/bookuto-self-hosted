<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model\Files;

/**
 * Interface ImageInterface
 * @package App\Model\Interfaces\Model\Files
 */
interface ImageInterface extends FileInterface
{

}
