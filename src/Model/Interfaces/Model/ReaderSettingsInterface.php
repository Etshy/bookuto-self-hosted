<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface ReaderSettingsTraitInterface
 * @package App\Model\Interfaces\Model
 */
interface ReaderSettingsInterface
{
    public function getReaderMode(): ?string;

    public function setReaderMode(?string $readerMode): void;

    public function getReaderDirection(): ?string;

    public function setReaderDirection(?string $readerDirection): void;
}
