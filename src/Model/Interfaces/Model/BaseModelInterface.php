<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model;

use DateTime;

/**
 * Interface BaseModelInterface
 * @package App\Model\Interfaces\Model
 */
interface BaseModelInterface
{
    public function getId(): null|string|int;

    public function setId(string|int $id): void;

    public function getCreatedAt(): ?DateTime;

    public function setCreatedAt(DateTime $createdAt): void;

    public function getUpdatedAt(): ?DateTime;

    public function setUpdatedAt(DateTime $updatedAt): void;
}
