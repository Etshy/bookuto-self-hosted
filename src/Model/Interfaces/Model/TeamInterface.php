<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Interfaces\Model\Files\ImageInterface;
use Doctrine\Common\Collections\Collection;

/**
 * Interface TeamInterface
 * @package App\Model\Interfaces\Model
 */
interface TeamInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 15;

    public function getName(): string;

    public function setName(string $name): void;

    public function getWebsite(): ?string;

    public function setWebsite(?string $website): void;

    public function getIrc(): ?string;

    public function setIrc(?string $irc): void;

    public function getDiscord(): ?string;

    public function setDiscord(?string $discord): void;

    public function getTwitter(): ?string;

    public function setTwitter(?string $twitter): void;

    public function getFacebook(): ?string;

    public function setFacebook(?string $facebook): void;

    public function getMembers(): ?Collection;

    public function setMembers(?Collection $members): void;

    public function getManagers(): ?Collection;

    public function setManagers(?Collection $managers): void;

    public function getChapters(): ?Collection;

    public function getSlug(): ?string;

    public function setSlug(string $slug): void;

    public function getSeries(): ?Collection;

    public function setImage(?ImageInterface $image): TeamInterface;

    public function getImage(): ?ImageInterface;
}
