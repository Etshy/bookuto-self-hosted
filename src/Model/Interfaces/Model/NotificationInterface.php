<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Persistence\User;

/**
 * Interface NotificationInterface
 * @package App\Model\Interfaces\Model
 */
interface NotificationInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 25;

    public function getUser(): User;

    public function setUser(User $user): void;

    public function isSent(): bool;

    public function setSent(bool $sent): void;

    public function isReaden(): bool;

    public function setReaden(bool $readen): void;
}
