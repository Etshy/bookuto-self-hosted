<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use App\Model\Interfaces\Model\Files\FileInterface;
use Doctrine\ODM\MongoDB\Repository\GridFSRepository;
use Doctrine\Persistence\ObjectManager;

/**
 * Interface FileRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface FileRepositoryInterface extends GridFSRepository
{
    public function getObjectManager(): ObjectManager;

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchRemove(array $ids): void;

    public function save(FileInterface $file): object;

    public function getFileStream(FileInterface $file);

    public function setFileInFS(FileInterface $file);
}
