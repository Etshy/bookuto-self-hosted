<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\TeamInterface;
use Iterator;
use Pagerfanta\Pagerfanta;

/**
 * Interfaces ChapterRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface ChapterRepositoryInterface extends RepositoryInterface
{
    public function findLatests(array $criteria): array;

    public function findOneByCriteria(array $criteria): ChapterInterface;

    public function findByCriteria(array $criteria): Iterator;

    public function getLatestsChaptersForTeam(TeamInterface $team, array $criteria): Iterator;

    public function findByCriteriaPaginated(array $criteria, int $page): Pagerfanta;

    public function findLatestsPaginated(int $page): Pagerfanta;

    public function isChapterAlreadyExists(array $criteria): bool;
}
