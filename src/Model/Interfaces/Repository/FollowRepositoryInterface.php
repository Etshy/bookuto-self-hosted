<?php


namespace App\Model\Interfaces\Repository;

use Doctrine\ODM\MongoDB\Iterator\Iterator;
use Pagerfanta\Pagerfanta;

/**
 * Interfaces FollowRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface FollowRepositoryInterface extends RepositoryInterface
{
    public function getPagination(array $criteria, int $page): Pagerfanta;

    public function getLastsForHome(array $criteria): Iterator;
}
