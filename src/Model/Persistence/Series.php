<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Application\Series\StatusManager;
use App\Application\Series\TagsManager;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\ReaderSettingsInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\SluggableInterface;
use App\Model\Persistence\Embed\ReaderSettings;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Series
 * @package App\Model\Persistence
 */
class Series extends BaseModel implements JsonSerializable, SeriesInterface, SluggableInterface
{
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    protected ?string $name;

    #[Assert\Type('string')]
    protected ?string $additionalNames = null;

    #[Assert\Type('string')]
    protected ?string $slug;

    protected ?ImageInterface $image = null;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    protected ?string $author;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    protected ?string $artist;

    protected ?string $description = null;

    protected bool $adult;

    protected bool $visible;

    #[Assert\Type('string')]
    protected ?string $customTitle = null;

    protected Collection $chapters;

    protected Collection $teams;

    #[Assert\Valid]
    protected ReaderSettingsInterface $readerSettings;

    protected ?DateTime $lastChapterPublishedAt = null;

    #[Assert\Choice(choices: TagsManager::TAGS, message: 'error.choose.valid.tag')]
    protected ?array $tags = null;

    #[Assert\Choice(choices: [StatusManager::STATUS_CANCELLED, StatusManager::STATUS_ON_HOLD, StatusManager::STATUS_ONGOING, StatusManager::STATUT_COMPLETED], message: 'error.choose.valid.status')]
    protected ?string $status = null;

    /**
     * Series constructor.
     */
    #[Pure]
    public function __construct()
    {
        $this->chapters = new ArrayCollection();
        $this->readerSettings = new ReaderSettings();
        $this->adult = false;
        $this->visible = false;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getAdditionalNames(): ?string
    {
        return $this->additionalNames;
    }

    public function setAdditionalNames(?string $additionalNames): void
    {
        $this->additionalNames = $additionalNames;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): void
    {
        $this->tags = $tags;
    }

    public function getImage(): ?ImageInterface
    {
        return $this->image;
    }

    public function setImage(?ImageInterface $image): void
    {
        $this->image = $image;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): void
    {
        $this->author = $author;
    }

    public function getArtist(): ?string
    {
        return $this->artist;
    }

    public function setArtist(?string $artist): void
    {
        $this->artist = $artist;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function isAdult(): bool
    {
        return $this->adult;
    }

    public function setAdult(bool $adult): void
    {
        $this->adult = $adult;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): void
    {
        $this->visible = $visible;
    }

    public function getCustomTitle(): ?string
    {
        return $this->customTitle;
    }

    public function setCustomTitle(?string $customTitle): void
    {
        $this->customTitle = $customTitle;
    }

    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function setTeams(Collection $teams): void
    {
        $this->teams = $teams;
    }

    public function getReaderSettings(): ReaderSettingsInterface
    {
        return $this->readerSettings;
    }

    public function setReaderSettings(ReaderSettingsInterface $readerSettings): void
    {
        $this->readerSettings = $readerSettings;
    }

    public function getLastChapterPublishedAt(): ?DateTime
    {
        return $this->lastChapterPublishedAt;
    }

    public function setLastChapterPublishedAt(?DateTime $lastChapterPublishedAt): void
    {
        $this->lastChapterPublishedAt = $lastChapterPublishedAt;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): Series
    {
        $this->status = $status;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->name,
            'slug' => $this->slug,
            'additionalNames' => $this->additionalNames,
            'author' => $this->author,
            'artist' => $this->artist,
            'description' => $this->description,
            'adult' => $this->adult,
            'visible' => $this->visible,
            'customTitle' => $this->customTitle,
            'lastChapterPublishedAt' => $this->lastChapterPublishedAt,
            'readerSettings' => $this->readerSettings->jsonSerialize(),
            'image' => ($this->getImage() instanceof ImageInterface) ? $this->getImage()->getId() : null,
        ];
    }

}
