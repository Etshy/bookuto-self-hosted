<?php
declare(strict_types=1);

namespace App\Model\Persistence\Files;

use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Model\Files\MetadataInterface;
use App\Model\Persistence\BaseModel;
use DateTime;
use JetBrains\PhpStorm\Pure;

/**
 * Class LocalFile
 * @package App\Model\Persistence\Files
 * Representation for files in filesystem
 */
class LocalFile extends BaseModel implements FileInterface
{
    protected string $name;
    protected DateTime $uploadDate;
    protected int $length;
    protected MetadataInterface $metadata;
    protected ?string $realPath;

    protected string $type;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    #[Pure]
    public function getUploadDate(): DateTime
    {
        //fix for first upload withouth uploadDate
        if (!isset($this->uploadDate)) {
            return $this->getCreatedAt();
        }
        return $this->uploadDate;
    }

    public function setUploadDate(DateTime $uploadDate): void
    {
        $this->uploadDate = $uploadDate;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function setLength(int $length): void
    {
        $this->length = $length;
    }

    public function getMetadata(): MetadataInterface
    {
        return $this->metadata;
    }

    public function setMetadata(MetadataInterface $fileMetadata): void
    {
        $this->metadata = $fileMetadata;
    }

    public function getRealPath(): ?string
    {
        return $this->realPath;
    }

    public function setRealPath(?string $realPath): void
    {
        $this->realPath = $realPath;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
