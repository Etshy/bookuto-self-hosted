<?php
declare(strict_types=1);


namespace App\Model\Persistence\Files;

use App\Model\Interfaces\Model\Files\MetadataInterface;

/**
 * Class FileMetadata
 * @package App\Model\Persistence\Files
 */
class FileMetadata implements MetadataInterface
{
    protected string $md5;
    protected string $mime;
    protected string $type = 'file';

    public function getMd5(): string
    {
        return $this->md5;
    }

    public function setMd5(string $md5): void
    {
        $this->md5 = $md5;
    }

    public function getMime(): string
    {
        return $this->mime;
    }

    public function setMime(string $mime): void
    {
        $this->mime = $mime;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
