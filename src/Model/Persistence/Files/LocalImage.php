<?php
declare(strict_types=1);

namespace App\Model\Persistence\Files;

use App\Model\Interfaces\Model\Files\ImageInterface;

/**
 * Class LocalImage
 * @package App\Model\Persistence\Files
 */
class LocalImage extends LocalFile implements ImageInterface
{

}
