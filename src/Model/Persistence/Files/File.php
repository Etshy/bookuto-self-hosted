<?php
declare(strict_types=1);

namespace App\Model\Persistence\Files;

use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Model\Files\MetadataInterface;
use DateTime;


/**
 * Class File
 * @package App\Model\Persistence\Files
 */
class File implements FileInterface
{
    protected string $id;
    protected string $name;
    /**
     * Not used but required by Doctrine (ODM, at least)
     */
    protected DateTime $uploadDate;
    protected int $length;
    protected int $chunkSize;
    protected MetadataInterface $metadata;
    protected ?string $realPath = null;

    public function getId(): int|string
    {
        return $this->id;
    }

    public function setId(string|int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUploadDate(): DateTime
    {
        return $this->uploadDate;
    }

    public function setUploadDate(DateTime $uploadDate): void
    {
        $this->uploadDate = $uploadDate;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function setLength(int $length): void
    {
        $this->length = $length;
    }

    public function getMetadata(): MetadataInterface
    {
        return $this->metadata;
    }

    public function setMetadata(MetadataInterface $fileMetadata): void
    {
        $this->metadata = $fileMetadata;
    }

    public function getRealPath(): ?string
    {
        return $this->realPath;
    }

    public function setRealPath(?string $realPath): void
    {
        $this->realPath = $realPath;
    }

    public function getChunkSize(): int
    {
        return $this->chunkSize;
    }

    public function setChunkSize(int $chunkSize): void
    {
        $this->chunkSize = $chunkSize;
    }

    public function toArray(): array
    {
        $properties = get_class_vars(self::class);
        $data = [];
        foreach ($properties as $attribut => $value) {
            $method = 'get'.str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
            if (is_callable([$this, $method])) {
                $data[$attribut] = $this->$method();
            }
        }

        return $data;
    }

    public function hydrate(array $data): void
    {
        foreach ($data as $attribut => $value) {
            $method = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
            if (is_callable([$this, $method])) {
                $this->$method($value);
            }
        }
    }
}
