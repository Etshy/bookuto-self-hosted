<?php
declare(strict_types=1);

namespace App\Model\Persistence\Embed;

use App\Model\Interfaces\Model\ReaderSettingsInterface;
use App\Model\Persistence\Traits\ReaderSettingsTrait;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

/**
 * Class ReaderSettings
 * @package App\Model\Persistence
 */
class ReaderSettings implements ReaderSettingsInterface, JsonSerializable
{
    use ReaderSettingsTrait;

    #[ArrayShape(['readerMode' => "null|string", 'readerDirection' => "null|string"])]
    public function jsonSerialize(): array
    {
        return [
            'readerMode' => $this->getReaderMode(),
            'readerDirection' => $this->getReaderDirection(),
        ];
    }
}
