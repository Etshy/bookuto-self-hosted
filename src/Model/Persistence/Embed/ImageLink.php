<?php
declare(strict_types=1);


namespace App\Model\Persistence\Embed;

/**
 * Class ImageLink
 * @package App\Model\Persistence
 * @use contain a source site and an uri from the source
 */
class ImageLink
{
    protected string $source;
    protected string $uri;

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

}
