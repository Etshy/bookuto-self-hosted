<?php
declare(strict_types=1);

namespace App\Model\Persistence\Embed;

use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\PageInterface;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

/**
 * Class Page
 * @package App\Model\Persistence
 *
 */
class Page implements JsonSerializable, PageInterface
{
    protected int $order;
    protected ImageInterface $image;
    protected ?ImageInterface $scrambledImage = null;
    protected Collection $imageLinks;

    /**
     * Page constructor.
     *
     */
    public function __construct()
    {
    }

    public function getOrder(): int
    {
        return $this->order;
    }

    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    public function getImage(): ImageInterface
    {
        return $this->image;
    }

    public function setImage(ImageInterface $image): void
    {
        $this->image = $image;
    }

    public function getScrambledImage(): ?ImageInterface
    {
        return $this->scrambledImage;
    }

    public function setScrambledImage(?ImageInterface $scrambledImage): void
    {
        $this->scrambledImage = $scrambledImage;
    }

    public function getImageLink(): Collection
    {
        return $this->imageLinks;
    }

    public function setImageLink(Collection $imageLink): void
    {
        $this->imageLinks = $imageLink;
    }

    public function addImageLink(ImageLink $imageLink): void
    {
        $this->imageLinks->add($imageLink);
    }

    #[ArrayShape(['order' => "int", 'image' => "string"])]
    public function jsonSerialize(): array
    {
        return [
            'order' => $this->order,
            'image' => $this->image->getName(),
        ];
    }

}
