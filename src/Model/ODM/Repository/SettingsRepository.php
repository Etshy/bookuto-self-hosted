<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Repository\SettingsRepositoryInterface;
use App\Model\Persistence\Settings;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * Class SettingsRepository
 * @package App\Model\Repository
 */
class SettingsRepository extends BaseRepository implements SettingsRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Settings::class);
    }
}
