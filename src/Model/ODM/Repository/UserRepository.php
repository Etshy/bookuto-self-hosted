<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Repository\UserRepositoryInterface;
use App\Model\Persistence\User;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\Query\Builder;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class UserRepository
 * @package App\Model\Repository
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, User::class);
    }

    public function findByCriteriaPaginated(array $criteria = [], int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        return $this->createPaginator($qb, $page);
    }

    private function buildCriteria(Builder $qb, array $criteria): Builder
    {
        return $qb;
    }

    private function createPaginator(Builder $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(User::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
