<?php
declare(strict_types=1);


namespace App\Model\ODM\Repository;

use App\Model\Persistence\Files\File;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * Class FileRepository
 * @package App\Model\Repository
 */
class FileRepository extends AbstractFileRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, File::class);
    }
}
