<?php
declare(strict_types=1);


namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Repository\LocalFileRepositoryInterface;
use App\Model\Persistence\Files\LocalFile;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * Class LocalFileRepository
 * @package App\Model\ODM\Repository
 */
class LocalFileRepository extends BaseRepository implements LocalFileRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, LocalFile::class);
    }
}
