<?php declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Model\ResetPasswordRequestInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\ResetPasswordRequestRepositoryInterface;
use App\Model\Persistence\ResetPasswordRequest;
use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\MongoDBException;

/**
 *
 */
class ResetPasswordRequestRepository extends BaseRepository implements ResetPasswordRequestRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, ResetPasswordRequest::class);
    }

    public function removeExpiredResetPasswordRequests(): void
    {
        $qb = $this->createQueryBuilder();
        $qb->remove()
            ->field('expiredAt')
            ->lt(new DateTime());
    }

    public function findResetPasswordRequest(string $selector): ?ResetPasswordRequestInterface
    {
        return $this->findOneBy(['selector' => $selector]);
    }

    /**
     * @throws MongoDBException
     */
    public function removeResetPasswordRequest(ResetPasswordRequestInterface $resetPasswordRequest): void
    {
        $this->getDocumentManager()->remove($resetPasswordRequest);
        $this->getDocumentManager()->flush();
    }

    /**
     * @throws MongoDBException
     */
    public function saveResetPasswordRequest(ResetPasswordRequestInterface $model): void
    {
        if (!$model->getId()) {
            $this->getDocumentManager()->persist($model);
        }
        $this->getDocumentManager()->flush();
    }

    public function getMostRecentNonExpiredRequestDate(UserInterface $user): ?DateTimeInterface
    {
        $qb = $this->createQueryBuilder();
        $qb->field('user')->references($user);
        $qb->limit(1);
        $qb->sort('requestedAt', self::SORT_DESC);
        /** @var ResetPasswordRequestInterface $resetPasswordRequest */
        $resetPasswordRequest = $qb->getQuery()->getSingleResult();

        if (null !== $resetPasswordRequest && !$resetPasswordRequest->isExpired()) {
            return $resetPasswordRequest->getRequestedAt();
        }

        return null;
    }
}
