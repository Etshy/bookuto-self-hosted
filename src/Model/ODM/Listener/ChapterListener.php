<?php
declare(strict_types=1);


namespace App\Model\ODM\Listener;

use App\Event\Event\ChapterPublished;
use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\DomainEventInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Service\SeriesService;
use DateTime;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ChapterListener
 * @package App\Model\ODM\Listener
 */
class ChapterListener implements EventSubscriber
{
    protected EventDispatcherInterface $dispatcher;

    protected bool $chapterPublished;

    protected ?ChapterInterface $chapter = null;

    protected SeriesService $seriesService;

    private DocumentManager $dm;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        $this->chapterPublished = false;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
            Events::postUpdate,
        ];
    }

    public function preUpdate(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();
        $this->dm = $eventArgs->getDocumentManager();
        if ($document instanceof ChapterInterface) {
            $this->processPreUpdateChapterChange($document);
        }
    }

    /**
     * @throws Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();
        $this->dm = $eventArgs->getDocumentManager();
        if ($document instanceof ChapterInterface) {
            $this->processPostUpdateChapterChange($document);
        }
    }

    public function processPreUpdateChapterChange(ChapterInterface $document): void
    {
        $chapterUpdated = false;
        $changeSet = $this->dm->getUnitOfWork()->getDocumentChangeSet($document);

        if ($document->getSeries() instanceof SeriesInterface && $document->getSeries()->isVisible()) {
            //if the visible property change
            if (array_key_exists('visible', $changeSet)) {
                $visibleChange = $changeSet['visible'];
                //If the property change from false to true
                if ($visibleChange[0] === false && $visibleChange[1] === true) {
                    $document->setPublishedAt(new DateTime());
                    $chapterUpdated = true;

                    //store data in class to be used on post flush
                    $this->chapterPublished = true;
                    $this->chapter = $document;
                }
            }
        } else {
            //If series not visible, force chapter as not visible
            $document->setVisible(false);
            $chapterUpdated = true;
        }

        if ($chapterUpdated) {
            //recompute the changes for the update
            $class = $this->dm->getClassMetadata(get_class($document));
            $this->dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
            if ($this->chapter instanceof ChapterInterface
                && $this->chapterPublished
                && $document instanceof DomainEventInterface
            ) {
                $document->raise(new ChapterPublished($document));
            }
        }
    }

    /**
     * @throws Exception
     */
    private function processPostUpdateChapterChange(ChapterInterface $document): void
    {
        if ($this->chapter instanceof ChapterInterface && $this->chapterPublished) {
            //Update series
            $this->chapter->getSeries()?->setLastChapterPublishedAt($this->chapter->getPublishedAt());
            if (!$this->chapter->getSeries() instanceof SeriesInterface) {
                throw new Exception();
            }
            $class = $this->dm->getClassMetadata(get_class($this->chapter->getSeries()));
            //Add/compute the series in the changeSet (this DOESN'T WORK in preUpdate)
            $this->dm->getUnitOfWork()->computeChangeSet($class, $this->chapter->getSeries());
        }
    }
}
