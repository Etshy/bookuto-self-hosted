<?php
declare(strict_types=1);


namespace App\Model\ODM\Listener;

use App\Model\Persistence\BaseModel;
use DateTime;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Event\PreUpdateEventArgs;
use Doctrine\ODM\MongoDB\Events;

/**
 * Class BaseDocumentListener
 * @package App\Listener
 */
class BaseDocumentListener implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();

        if ($document instanceof BaseModel) {
            $document->setCreatedAt(new DateTime());
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();

        if ($document instanceof BaseModel) {
            $dm = $eventArgs->getDocumentManager();
            $document->setUpdatedAt(new DateTime());
            $class = $dm->getClassMetadata(get_class($document));
            $dm->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
        }
    }
}
