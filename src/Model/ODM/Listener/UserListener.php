<?php
declare(strict_types=1);


namespace App\Model\ODM\Listener;

use App\Event\Event\UserEvents;
use App\Model\Interfaces\Model\UserInterface;
use App\Security\SecurityService;
use Doctrine\Bundle\MongoDBBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class UserListener
 * @package App\Model\ODM\Listener
 */
class UserListener implements EventSubscriberInterface
{
    protected EventDispatcherInterface $dispatcher;
    protected bool $emailChanged = false;
    protected bool $languageChanged = false;
    protected ?UserInterface $user = null;
    protected TranslatorInterface $translator;
    protected SecurityService $sercurityService;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        TranslatorInterface $translator,
        SecurityService $sercurityService
    ) {
        $this->dispatcher = $dispatcher;
        $this->translator = $translator;
        $this->sercurityService = $sercurityService;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
            Events::postFlush,
        ];
    }

    public function preUpdate(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();

        if ($document instanceof UserInterface) {
            $changeSet = $eventArgs->getDocumentManager()->getUnitOfWork()->getDocumentChangeSet($document);

            if (array_key_exists('email', $changeSet)) {
                //flag to indicate the email changed
                $this->emailChanged = $changeSet['email'][0] !== $changeSet['email'][1];
            }
            if (array_key_exists('language', $changeSet)) {
                $this->languageChanged = $changeSet['language'][0] !== $changeSet['language'][1];
            }
            $this->user = $document;
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function postFlush(): void
    {
        if ($this->emailChanged) {
            //the email changed, send confirmation email
            $this->sercurityService->sendConfirmationEmail($this->user);
        }
        if ($this->languageChanged) {
            $this->dispatcher->dispatch(new UserEvents($this->user), UserEvents::USER_LANGUAGE_CHANGED);
        }
    }
}
