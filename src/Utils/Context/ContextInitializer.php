<?php declare(strict_types=1);

namespace App\Utils\Context;

use App\Service\SettingsService;

class ContextInitializer
{
    protected ContextAccessor $contextAccessor;
    protected SettingsService $settingsService;

    public function __construct(
        ContextAccessor $contextAccessor,
        SettingsService $settingsService
    ) {
        $this->contextAccessor = $contextAccessor;
        $this->settingsService = $settingsService;
    }

    public function initialize(): void
    {
        $context = new Context();
        $context->setSettings($this->settingsService->findSettings());
        $this->contextAccessor->initContext($context);
    }
}
