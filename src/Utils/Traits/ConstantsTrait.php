<?php
declare(strict_types=1);

namespace App\Utils\Traits;

use ReflectionClass;
use ReflectionClassConstant;

/**
 * Class ConstantsTrait
 * @package App\Utils\Traits
 */
trait ConstantsTrait
{

    /**
     * @return ReflectionClassConstant[]
     */
    public function getConstants(bool $useObject = false): array
    {
        $oClass = new ReflectionClass(__CLASS__);
        if ($useObject) {
            return $oClass->getReflectionConstants();
        }

        return $oClass->getConstants();
    }

    public function getConstant(string $name, bool $useObject = false): ?ReflectionClassConstant
    {
        $oClass = new ReflectionClass(__CLASS__);
        if ($oClass->hasConstant($name)) {
            if ($useObject) {
                return $oClass->getReflectionConstant($name);
            }

            return $oClass->getConstant($name);
        }

        return null;
    }
}
