<?php declare(strict_types=1);

namespace App\Twig;

use DateTimeImmutable;
use DateTimeInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TimeDiffExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('timeDiff', [$this, 'timeDiff']),
        ];
    }

    public function timeDiff(DateTimeInterface $date, string $difference = 'days'): int
    {
        return $date->diff(new DateTimeImmutable())->{$difference};
    }

}
