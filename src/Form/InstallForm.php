<?php declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Languages;
use Symfony\Contracts\Translation\TranslatorInterface;

class InstallForm extends AbstractType
{
    public const DATABASE_TYPE = 'database_type'; // Select mongodb or orm => DB_DRIVER
    public const DATABASE_CONNECTION_STRING = 'database_connection_string'; // => MONGODB_URL or DATABASE_URL
    public const DATABASE_NAME = 'database_name'; // => MONGODB_DB or end of DATABASE_URL (DATABASE_URL . '/' . DATABASE_NAME)
    public const MAILER_DSN = 'mailer_dsn'; // smtp URI (smtp://user:pass@smtp.example.com:25) => MAILER_DSN
    public const MAIL_SENDER_ADDRESS = 'mail_sender_address'; // EMAIL_SENDER_ADDRESS
    public const MAIL_SENDER_NAME = 'mail_sender_mail'; // EMAIL_SENDER_NAME
    public const DEFAULT_LOCALE = 'default_locale'; // fr or en for now => DEFAULT_LOCALE


    public const DATABASE_MONGO = 'mongodb';
    public const DATABASE_ORM = 'orm';

    protected TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    public function buildForm(FormBuilderInterface $builder, array $options):void
    {
        $builder->add(
            self::DATABASE_TYPE,
            ChoiceType::class,
            [
                'choices' => [
                    $this->translator->trans('label.mongodb') => 'mongodb',
                    $this->translator->trans('label.orm') => 'orm' ,
                ],
                'choice_attr' => [
                    $this->translator->trans('label.orm') => [
                        'disabled' => true,
                    ]
                ],
                'placeholder' => $this->translator->trans('label.database_type.choose'),
                'label' => $this->translator->trans('label.database_type.label'),
                'multiple' => false,
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'required' => true,
                'help' => $this->translator->trans('label.database_type.help'),
            ]
        )->add(
            self::DATABASE_CONNECTION_STRING,
            TextType::class,
            [
                'required' => true,
                'label' => $this->translator->trans('label.database_connection_string.label'),
                'help' => $this->translator->trans('label.database_connection_string.help'),
                'attr' => [
                    'class' => 'ui inverted',
                ],
            ]
        )->add(
            self::DATABASE_NAME,
            TextType::class,
            [
                'required' => true,
                'label' => $this->translator->trans('label.database_name.label'),
                'help' => $this->translator->trans('label.database_name.help'),
            ]
        )->add(
            self::MAILER_DSN,
            TextType::class,
            [
                'required' => true,
                'label' => $this->translator->trans('label.mailer_dsn.label'),
                'help' => $this->translator->trans('label.mailer_dsn.help'),
            ]
        )->add(
            self::MAIL_SENDER_ADDRESS,
            TextType::class,
            [
                'required' => true,
                'label' => $this->translator->trans('label.mail_sender_address.label'),
                'help' => $this->translator->trans('label.mail_sender_address.help'),
            ]
        )->add(
            self::MAIL_SENDER_NAME,
            TextType::class,
            [
                'required' => true,
                'label' => $this->translator->trans('label.mail_sender_name.label'),
                'help' => $this->translator->trans('label.mail_sender_name.help'),
            ]
        )->add(
            self::DEFAULT_LOCALE,
            ChoiceType::class,
            [
                'choices' => [
                    Languages::getName('en', ) => 'en',
                    Languages::getName('fr', ) => 'fr',
                ],
                'placeholder' => $this->translator->trans('label.default_locale.choose'),
                'label' => $this->translator->trans('label.default_locale.label'),
                'help' => $this->translator->trans('label.default_locale.help'),
                'multiple' => false,
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'required' => true,
            ]
        );
    }
}
