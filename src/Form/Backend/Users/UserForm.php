<?php
declare(strict_types=1);

namespace App\Form\Backend\Users;

use App\Model\Interfaces\Model\UserInterface;
use App\Security\RoleService;
use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserForm
 * @package App\Form\Backend\Users
 */
class UserForm extends AbstractType
{
    public const ROLES = 'roles';

    private RoleService $roleService;

    /**
     * UserForm constructor.
     *
     * @param RoleService $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }


    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $builder->getData();
        if (!$user instanceof UserInterface) {
            throw new Exception('Invalid User');
        }

        $roles = $options['roles'];

        $builder->add(self::ROLES, ChoiceType::class, [
            'choices' => $roles,
            'label' => 'label.roles',
            'attr' => [
                'class' => 'ui search dropdown inverted tiny',
            ],
            'data' => $user->getRoles(),
            'multiple' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $roles = $this->roleService->getAllRolesForForm();

        $resolver->setDefaults([
            'roles' => $roles,
        ]);
    }
}
