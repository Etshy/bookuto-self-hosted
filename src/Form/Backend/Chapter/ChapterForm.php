<?php
declare(strict_types=1);

namespace App\Form\Backend\Chapter;

use App\Model\Persistence\Chapter;
use App\Model\Persistence\Series;
use App\Model\Persistence\Team;
use App\Voter\ChapterVoter;
use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class ChapterForm
 * @package App\Form\Backend\Chapter
 */
class ChapterForm extends AbstractType
{
    public const NAME = 'name';
    public const SERIES = 'series';
    public const NUMBER = 'number';
    public const VISIBLE = 'visible';
    public const LANGUAGE = 'language';
    public const TEAMS = 'teams';

    /**
     * @var AuthorizationCheckerInterface
     */
    private AuthorizationCheckerInterface $authorizationChecker;

    /**
     * SeriesForm constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array{seriesId:string|null} $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $chapter = $builder->getData();
        /**
         * @var Chapter $chapter
         */
        $disabled = false;
        //If the user doesn't have the rights to edit chapter
        if (!$this->authorizationChecker->isGranted(ChapterVoter::ROLE_EDIT_CHAPTER, $chapter)) {
            $disabled = true;
        }

        //a series is already set, or we add a chapter from a specific series = we can't choose the series
        $seriesDisabled = false;
        if ($chapter->getSeries() instanceof Series || (array_key_exists('seriesId', $options) && !is_null($options['seriesId']))) {
            $seriesDisabled = true;
        }

        $builder->add('id', HiddenType::class)
            ->add(
                self::NAME,
                TextType::class,
                [
                    'label' => 'label.name',
                    'disabled' => $disabled,
                    'required' => false,
                ]
            )
            ->add(self::SERIES, DocumentType::class, [
                'class' => Series::class,
                'choice_label' => 'name',
                'required' => true,
                'label' => 'label.series.name',
                'placeholder' => 'label.series.choose',
                'attr' => [
                    'class' => 'ui dropdown',
                ],
                'disabled' => $seriesDisabled,
            ])
            ->add(self::NUMBER, NumberType::class, [
                'label' => 'label.number',
                'required' => true,
                'disabled' => $disabled,
                'help' => 'label.chapter_number.help',
            ])
            ->add(self::LANGUAGE, LanguageType::class, [
                'label' => 'label.language',
                'placeholder' => 'label.language_choose',
                'attr' => [
                    'class' => 'ui inverted search dropdown',
                ],
                'disabled' => $disabled,
                'required' => true,
            ])
            ->add(self::TEAMS, DocumentType::class, [
                'class' => Team::class,
                'choice_label' => 'name',
                'required' => true,
                'label' => 'label.teams',
                'placeholder' => 'label.teams_choose',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
                'multiple' => true,
                'disabled' => $disabled,
            ]);

        if ($chapter->getId()) {
            $builder->add(self::VISIBLE, CheckboxType::class, [
                'label' => 'label.chapter_visible',
                'disabled' => $disabled,
                'required' => false,
                'help' => 'label.chapter_visible_help',
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'seriesId' => null,
        ]);
    }
}
