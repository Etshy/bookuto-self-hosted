<?php
declare(strict_types=1);

namespace App\Form\Backend\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SettingsBaseForm
 * @package App\Form\Backend\Profile
 */
class SettingsBaseForm extends AbstractType
{
    public const SETTINGS = 'settings';
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(self::SETTINGS, SettingsForm::class);
    }
}
