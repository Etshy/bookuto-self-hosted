<?php
declare(strict_types=1);

namespace App\Form\Backend\Profile;

use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Persistence\Embed\UserSettings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PreferencesForm
 * @package App\Form\Backend\Profile
 */
class SettingsForm extends AbstractType
{
    public const EMAIL_CHAPTER_EMAIL_NOTIFICATION = 'chapterEmailNotificationActivated';
    public const READER_MODE = 'readerMode';
    public const READER_DIRECTION = 'readerDirection';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(self::EMAIL_CHAPTER_EMAIL_NOTIFICATION, CheckboxType::class, [
            'required' => false,
            'label' => 'label.activate-email',
        ])
            ->add(self::READER_MODE, ChoiceType::class, [
                'choices' => [
                    'label.reader_mode.classic' => SeriesInterface::READER_MODE_CLASSIC_VALUE,
                    'label.reader_mode.webtoon' => SeriesInterface::READER_MODE_WEBTOON_VALUE,
                ],
                'required' => false,
                'placeholder' => 'label.reader_mode.default.choose',
                'label' => 'label.reader_mode.default',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
            ])
            ->add(
                self::READER_DIRECTION,
                ChoiceType::class,
                [
                    'choices' => [
                        'label.reader_direction.ltr' => SeriesInterface::READER_DIRECTION_LTR_VALUE,
                        'label.reader_direction.rtl' => SeriesInterface::READER_DIRECTION_RTL_VALUE,
                    ],
                    'required' => false,
                    'placeholder' => 'label.reader_direction.default.choose',
                    'label' => 'label.reader_direction.default',
                    'attr' => [
                        'class' => 'ui inverted dropdown',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserSettings::class, // "link" this form to UserSettings class
        ]);
    }
}
