<?php
declare(strict_types=1);

namespace App\Form\Backend\Profile;

use App\Form\Types\UploadedFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProfileForm
 * @package App\Form\Backend\User
 */
class ProfileForm extends AbstractType
{
    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const LANGUAGE = 'language';
    public const BIOGRAPHY = 'biography';
    public const AVATAR = 'avatar';
    public const PRIVATE = 'private';
    public const WEBSITE = 'website';
    public const DISCORD_PROFILE = 'discordProfile';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setMethod('PATCH');
        $builder
            ->add(self::USERNAME, TextType::class, [
                'disabled' => true,
                'label' => 'label.username',
            ])
            ->add(self::EMAIL, EmailType::class, [
                'required' => false,
                'row_attr' => [
                    'class' => ' ui inverted transparent input  ',
                ],
                'label' => 'label.email',
            ])
            ->add(self::LANGUAGE, LanguageType::class, [
                'placeholder' => 'language.choose',
                'preferred_choices' => [
                    'en',
                    'fr',
                ],
                'attr' => [
                    'class' => 'ui search clearable dropdown inverted tiny',
                ],
                'required' => false,
                'label' => 'label.language',
            ])
            ->add(
                self::AVATAR,
                UploadedFileType::class,
                [
                    'required' => false,
                    'label' => 'label.avatar',
                ]
            )
            ->add(self::WEBSITE, TextType::class, [
                'required' => false,
                'label' => 'label.website',
            ])
            ->add(self::DISCORD_PROFILE, TextType::class, [
                'required' => false,
                'label' => 'label.discord',
            ])
            ->add(self::PRIVATE, CheckboxType::class, [
                'required' => false,
                'label' => 'label.private',
            ])
            ->add(self::BIOGRAPHY, TextareaType::class, [
                'required' => false,
                'label' => 'label.biography',
            ])
        ;
    }

}
