<?php
declare(strict_types=1);

namespace App\Form\Backend\Settings;

use App\Form\Types\UploadedFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class GeneralSettingsForm
 * @package App\Form\Backend\Settings
 */
class GeneralSettingsForm extends AbstractType
{
    public const LOGO = 'logo';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('id', HiddenType::class)
            ->add(
                self::LOGO,
                UploadedFileType::class,
                [
                    'required' => false,
                    'label' => 'label.application_logo',
                ]
            );
    }
}
