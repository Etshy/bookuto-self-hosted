<?php
declare(strict_types=1);

namespace App\Controller\Query;

use App\Model\Criteria\SeriesCriteria;

/**
 * Class SeriesFiltersQuery
 * @package App\Controller\Query
 */
class SeriesFiltersQuery extends SeriesCriteria implements QueryInterface
{
}
