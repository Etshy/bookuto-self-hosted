<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Form\Backend\Settings\GeneralSettingsForm;
use App\Form\Backend\Settings\ReaderSettingsForm;
use App\Model\Interfaces\Model\SettingsInterface;
use App\Model\Persistence\Settings;
use App\Service\SettingsService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SettingsController
 * @package App\Controller\Backend
 */
#[Route('/admin/settings', name: 'admin_settings_')]
class SettingsController extends AbstractController
{

    protected TranslatorInterface $translator;
    private SettingsService $settingsService;

    /**
     * SettingsController constructor.
     *
     * @param SettingsService $settingsService
     * @param TranslatorInterface $translator
     */
    public function __construct(SettingsService $settingsService, TranslatorInterface $translator)
    {
        $this->settingsService = $settingsService;
        $this->translator = $translator;
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/general', name: 'general')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_GENERAL_SETTINGS')")]
    public function general(Request $request): Response
    {
        $settings = $this->settingsService->findAll();

        if (is_array($settings) && count($settings) > 0) {
            $settings = $settings[0];
        }

        if (!$settings instanceof SettingsInterface) {
            $settings = new Settings();
        }

        $form = $this->createForm(GeneralSettingsForm::class, $settings);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->settingsService->save($settings);
            $this->addFlash(
                'success',
                $this->translator->trans('settings.successfully.edited')
            );
        }

        return $this->render('backend/settings/general.html.twig', [
            'form' => $form->createView(),
            'settings' => $settings,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RealPathRequiredException
     */
    #[Route('/delete-logo', name: 'delete_logo')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_TEAM')")]
    public function deleteLogo(): RedirectResponse
    {
        $settings = $this->settingsService->findAll();
        if (is_array($settings) && count($settings) > 0) {
            $settings = $settings[0];
        }
        /** @var SettingsInterface $settings */
        $settings->setLogo(null);
        $this->settingsService->save($settings);

        return $this->redirect($this->generateUrl('admin_settings_general'));
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/reader', name: 'reader')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_READER_SETTINGS')")]
    public function reader(Request $request): Response
    {
        $settings = $this->settingsService->findAll();

        if (is_array($settings) && count($settings) > 0) {
            $settings = $settings[0];
        }

        if (!$settings instanceof Settings) {
            $settings = new Settings();
        }

        $form = $this->createForm(ReaderSettingsForm::class, $settings);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->settingsService->save($settings);
            $this->addFlash(
                'success',
                $this->translator->trans('settings.successfully.edited')
            );
        }

        return $this->render('backend/settings/reader.html.twig', [
            'form' => $form->createView(),
            'settings' => $settings,
        ]);
    }

    #[Route('/check-update', name: 'check_update')]
    #[Security("is_granted('ROLE_ADMIN')")]
    public function checkUpdate()
    {
        //TODO get latest tag on gitlab and check if different than current version

    }
}
