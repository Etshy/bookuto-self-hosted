<?php declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Service\ChapterService;
use App\Utils\Context\ContextAccessor;
use Etshy\Bundle\PhpZipBundle\Services\PhpZipInterface;
use PhpZip\Exception\ZipException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DownloadChapterController extends AbstractController
{
    protected ContextAccessor $contextAccessor;
    protected ChapterService $chapterService;
    protected PhpZipInterface $phpZipService;
    protected TranslatorInterface $translator;

    public function __construct(
        ContextAccessor $contextAccessor,
        ChapterService $chapterService,
        PhpZipInterface $phpZipService,
        TranslatorInterface $translator
    ) {
        $this->contextAccessor = $contextAccessor;
        $this->chapterService = $chapterService;
        $this->phpZipService = $phpZipService;
        $this->translator = $translator;
    }

    #[Route('/download/{id}', name: 'download_chapter')]
    public function download(string $id, Request $request): RedirectResponse|Response
    {
        $settings = $this->contextAccessor->getContext()->getSettings();

        if (!$settings->isDownloadEnabled()) {
            $url = $this->generateRedirectUrl($request);

            return new RedirectResponse($url);
        }

        if ($settings->isDownloadProtected() && !$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $url = $this->generateRedirectUrl($request);

            return new RedirectResponse($url);
        }

        $chapter = $this->chapterService->find($id);

        if (!$chapter instanceof ChapterInterface) {
            $url = $this->generateRedirectUrl($request);

            return new RedirectResponse($url);
        }

        $path = $this->chapterService->getChapterImageFolder($chapter);
        $zipFileName = $chapter->getSeries()?->getName();
        $zipFileName .= '_'.($chapter->getSeries()?->getCustomTitle()) ?: $this->translator->trans('label.chapter');
        $zipFileName .= '_'.$chapter->getNumber().'.zip';
        $phpZip = $this->phpZipService->createZip();
        try {
            $phpZip->addDir($path);

            return $phpZip->outputAsSymfonyResponse($zipFileName);
        } catch (ZipException) {
            $url = $this->generateRedirectUrl($request);

            return new RedirectResponse($url);
        }
    }

    private function generateRedirectUrl(Request $request)
    {
        if ($request->server->has('HTTP_REFERER')) {
            $url = $request->server->get('HTTP_REFERER');
        } else {
            //if no referrer redirect to homepage
            $url = $this->generateUrl('homepage');
        }

        return $url;
    }
}
