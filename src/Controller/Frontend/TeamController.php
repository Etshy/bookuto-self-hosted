<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Model\Interfaces\Model\ChapterReadenInterface;
use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\ChapterReadenService;
use App\Service\ChapterService;
use App\Service\TeamService;
use App\Utils\Context\ContextAccessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TeamController
 * @package App\Controller\Frontend
 */
#[Route('/team', name: 'public_team_')]
class TeamController extends AbstractController
{
    protected TeamService $teamService;
    protected ChapterService $chapterService;
    protected ChapterReadenService $chapterReadenService;
    protected ContextAccessor $contextAccessor;

    /**
     * @param TeamService $teamService
     * @param ChapterService $chapterService
     * @param ChapterReadenService $chapterReadenService
     * @param ContextAccessor $contextAccessor
     */
    public function __construct(
        TeamService $teamService,
        ChapterService $chapterService,
        ChapterReadenService $chapterReadenService,
        ContextAccessor $contextAccessor
    ) {
        $this->teamService = $teamService;
        $this->chapterService = $chapterService;
        $this->chapterReadenService = $chapterReadenService;
        $this->contextAccessor = $contextAccessor;
    }

    #[Route('/{page}', name: 'list', requirements: ['page' => '\d+'], defaults: ['page' => 1])]
    public function list(int $page = 1): Response
    {
        $criteria = [
            'visible' => true,
        ];
        $pagination = $this->teamService->findByCriteriaPaginated($criteria, $page);

        return $this->render('frontend/team/list.html.twig', [
            'pagination' => $pagination,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }

    #[Route('/{slug}', name: 'details', requirements: ['slug' => '[a-zA-Z0-9_-]+'])]
    public function details(string $slug): Response
    {
        $team = $this->teamService->findOneBy([
            'slug' => $slug,
        ]);

        if (!$team instanceof TeamInterface) {
            throw $this->createNotFoundException();
        }

        $latestsChapters = $this->chapterService->getLatestsChaptersForTeam($team);

        $user = $this->getUser();

        $idsChapterRead = [];
        if ($user instanceof UserInterface) {
            $chaptersRead = $this->chapterReadenService->findBy([
                'user.id' => $user->getId(),
            ]);
            foreach ($chaptersRead as $chapterRead) {
                if (!$chapterRead instanceof ChapterReadenInterface) {
                    continue;
                }
                $idsChapterRead[] = $chapterRead->getChapter()->getId();
            }
        }

        return $this->render('frontend/team/details.html.twig', [
            'team' => $team,
            'latestsChapters' => $latestsChapters,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
            'idsChaptersRead' => $idsChapterRead,
        ]);
    }
}
