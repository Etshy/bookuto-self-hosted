<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Model\Interfaces\Model\ChapterReadenInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\ChapterReadenService;
use App\Service\ChapterService;
use App\Service\FollowService;
use App\Utils\Context\ContextAccessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\Frontend
 */
class IndexController extends AbstractController
{
    protected ContextAccessor $contextAccessor;
    private ChapterService $chapterService;
    private ChapterReadenService $chapterReadService;
    private FollowService $followService;

    public function __construct(
        ChapterService $chapterService,
        ChapterReadenService $chapterReadService,
        FollowService $followService,
        ContextAccessor $contextAccessor,
    ) {
        $this->chapterService = $chapterService;
        $this->chapterReadService = $chapterReadService;
        $this->followService = $followService;
        $this->contextAccessor = $contextAccessor;
    }

    #[Route('', name: 'homepage')]
    public function index(): Response
    {
        $pagination = $this->chapterService->findLatestsPaginated();
        $user = $this->getUser();

        $idsChapterRead = [];
        if ($user instanceof UserInterface) {
            $chaptersRead = $this->chapterReadService->findBy([
                'user.id' => $user->getId(),
            ]);
            foreach ($chaptersRead as $chapterRead) {
                if (!$chapterRead instanceof ChapterReadenInterface) {
                    continue;
                }
                $idsChapterRead[] = $chapterRead->getChapter()->getId();
            }
        }

        $follows = $this->followService->getLastsForHome([
            'user' => $user,
        ]);

        return $this->render('frontend/index/index.html.twig', [
            'pagination' => $pagination,
            'idsChaptersRead' => $idsChapterRead,
            'follows' => $follows,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }

    #[Route('/latest/{page}', name: 'latest', requirements: ['id' => '\d+'], defaults: ['page' => 1])]
    public function latest(int $page): Response
    {
        $pagination = $this->chapterService->findLatestsPaginated($page);

        return $this->render('frontend/index/index.html.twig', [
            'pagination' => $pagination,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }
}
