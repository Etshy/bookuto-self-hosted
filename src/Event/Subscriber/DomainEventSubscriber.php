<?php declare(strict_types=1);

namespace App\Event\Subscriber;

use App\Model\ODM\Listener\DomainEventCollector;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class DomainEventSubscriber
 * @package App\Event\Subscriber
 */
class DomainEventSubscriber implements EventSubscriberInterface
{
    protected DomainEventCollector $domainEventsCollector;

    public function __construct(DomainEventCollector $domainEventsCollector)
    {
        $this->domainEventsCollector = $domainEventsCollector;
    }

    #[ArrayShape([KernelEvents::RESPONSE => "string"])]
    public static function getSubscribedEvents():array
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelResponse(): void
    {
        if ($this->domainEventsCollector->hasUndispatchedEvents()) {
            $this->domainEventsCollector->dispatchCollectedEvents();
        }
    }
}
