<?php
declare(strict_types=1);

namespace App\Voter;

use App\Model\Interfaces\Model\UserInterface;
use App\Model\Persistence\User;
use App\Utils\Traits\ConstantsTrait;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserVoter
 * @package App\Voter
 */
class UserVoter extends Voter
{
    use ConstantsTrait;

    //Users Roles
    public const ROLE_LIST_USERS = 'ROLE_LIST_USERS';
    public const ROLE_ADD_USER = 'ROLE_ADD_USER';
    public const ROLE_VIEW_USER = 'ROLE_VIEW_USER';
    public const ROLE_EDIT_USER = 'ROLE_EDIT_USER';
    public const ROLE_DELETE_USER = 'ROLE_DELETE_USER';

    public const ROLE_ADMIN = 'ROLE_ADMIN';

    //Access to roles/groups page
    public const ROLE_EDIT_USER_ROLE = 'ROLE_EDIT_USER_ROLE';

    /**
     * @var AccessDecisionManagerInterface
     */
    private AccessDecisionManagerInterface $decisionManager;

    /**
     * SeriesVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        //$constants = $this->getConstants();
        $constants = [
            self::ROLE_LIST_USERS,
            self::ROLE_ADD_USER,
            self::ROLE_VIEW_USER,
            self::ROLE_EDIT_USER,
            self::ROLE_DELETE_USER,
            self::ROLE_EDIT_USER_ROLE,
        ];

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $constants)) {
            return false;
        }

        //For add and list perms, we don't have a series to control, so we bypass that.
        if (!in_array($attribute, [self::ROLE_ADD_USER, self::ROLE_LIST_USERS]) && !$subject instanceof User) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {

        //ADMIN and SUPER_ADMIN can do anything they want !
        if ($this->decisionManager->decide($token, [UserVoter::ROLE_ADMIN], $subject)) {
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            // the user must be logged in. if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::ROLE_ADD_USER:
            case self::ROLE_VIEW_USER:
            case self::ROLE_DELETE_USER:
            case self::ROLE_EDIT_USER_ROLE:
            case self::ROLE_EDIT_USER:
            case self::ROLE_LIST_USERS:
                if (in_array($attribute, $user->getRoles())) {
                    return true;
                }
                break;

            default:
                throw new LogicException('This code should not be reached!');
        }

        return false;
    }
}
