<?php
declare(strict_types=1);

namespace App\Voter;

use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Persistence\Series;
use App\Utils\Traits\ConstantsTrait;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class SeriesVoter
 * @package App\Voter
 */
class SeriesVoter extends Voter
{
    use ConstantsTrait;

    //Access to series list page
    public const ROLE_LIST_SERIES = 'ROLE_LIST_SERIES';
    public const ROLE_ADD_SERIES = 'ROLE_ADD_SERIES';
    public const ROLE_DELETE_SERIES = 'ROLE_DELETE_SERIES';
    public const ROLE_EDIT_SERIES = 'ROLE_EDIT_SERIES';
    public const ROLE_VIEW_SERIES = 'ROLE_VIEW_SERIES';

    private AccessDecisionManagerInterface $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        //$constants = $this->getConstants();
        $constants = [
            self::ROLE_LIST_SERIES,
            self::ROLE_ADD_SERIES,
            self::ROLE_DELETE_SERIES,
            self::ROLE_EDIT_SERIES,
            self::ROLE_VIEW_SERIES,
        ];

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $constants)) {
            return false;
        }

        //For add and list perms, we don't have a series to control, so we bypass that.
        if (!in_array($attribute, [self::ROLE_ADD_SERIES, self::ROLE_LIST_SERIES]) && !$subject instanceof SeriesInterface) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        //ADMIN and SUPER_ADMIN can do anything they want !
        if ($this->decisionManager->decide($token, [UserVoter::ROLE_ADMIN])) {
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            // the user must be logged in. if not, deny access
            return false;
        }

        $series = $subject;
        switch ($attribute) {
            case self::ROLE_ADD_SERIES:
            case self::ROLE_LIST_SERIES:
                if (in_array($attribute, $user->getRoles())) {
                    return true;
                }
                break;

            case self::ROLE_VIEW_SERIES:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canView($series, $user);
                }
                break;

            case self::ROLE_DELETE_SERIES:
            case self::ROLE_EDIT_SERIES:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canEdit($series, $user);
                }
                break;

            default:
                throw new LogicException('This code should not be reached!');
        }

        return false;
    }

    private function canView(SeriesInterface $series, UserInterface $user): bool
    {
        return $this->canEdit($series, $user);
    }

    private function canEdit(SeriesInterface $series, UserInterface $user): bool
    {
        if ($series instanceof Series) {
            return $this->userIsManagerOfSeries($series, $user);
        }

        return false;
    }

    private function userIsManagerOfSeries(SeriesInterface $series, UserInterface $user): bool
    {
        $teams = $series->getTeams();

        //We search in the Teams related to the Series
        foreach ($teams as $team) {
            if (!$team instanceof TeamInterface) {
                continue;
            }

            //We search in the Managers of the team
            $members = $team->getManagers();

            foreach ($members as $member) {
                if (!$member instanceof UserInterface) {
                    continue;
                }
                //if the current User is in the managers, he can update it
                if ($member->getId() === $user->getId()) {
                    return true;
                }
            }
        }

        return false;
    }
}
