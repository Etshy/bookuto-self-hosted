<?php
declare(strict_types=1);

namespace App\Exceptions\Services\Notification;

use Exception;

/**
 * Class UnsupportedNotificationTypeException
 * @package App\Exceptions\Services\Notification
 */
class UnsupportedNotificationTypeException extends Exception
{
}
