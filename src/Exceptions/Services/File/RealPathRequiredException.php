<?php declare(strict_types=1);

namespace App\Exceptions\Services\File;

use Exception;

/**
 * Class RealPathRequiredException
 * @package App\Exceptions\Services\File
 */
class RealPathRequiredException extends Exception
{
}
