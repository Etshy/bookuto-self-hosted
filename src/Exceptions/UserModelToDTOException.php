<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class UserModelToDTOException
 * @package App\Exceptions
 */
class UserModelToDTOException extends Exception
{
}
