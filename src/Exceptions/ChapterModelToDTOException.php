<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class ChapterModelToDTOException
 * @package App\Exceptions
 */
class ChapterModelToDTOException extends Exception
{
}
