<?php declare(strict_types=1);

namespace App\Exceptions\Repository;

use Exception;

/**
 * Class NoResultException
 * @package App\Exceptions\Repository
 */
class NoResultException extends Exception
{
}
