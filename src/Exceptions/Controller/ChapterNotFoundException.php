<?php declare(strict_types=1);

namespace App\Exceptions\Controller;

use Exception;

/**
 * Class ChapterNotFoundException
 * @package App\Exceptions\Controller
 */
class ChapterNotFoundException extends Exception
{
}
