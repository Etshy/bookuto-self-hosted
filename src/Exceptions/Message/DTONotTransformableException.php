<?php
declare(strict_types=1);

namespace App\Exceptions\Message;

use Exception;

/**
 * Class DTONotTransformableException
 * @package App\Exceptions\Message
 */
class DTONotTransformableException extends Exception
{
}
