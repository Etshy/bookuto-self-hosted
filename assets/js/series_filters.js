$(function() {

  $('#toggle_filters').on('click', function() {
    $('#series_filters_form_container').slideToggle('fast', function() {
      if ($('#series_filters_form_container').is(':visible')) {
        $('#toggle_filters>i').removeClass('down').addClass('up');
      } else {
        $('#toggle_filters>i').removeClass('up').addClass('down');
      }
    });
  });

  $('#include_tags').on('change', function() {
    let selectedValues = $(this).val();
    $('#exclude_tags>option').prop('disabled', false);
    $('#exclude_tags').closest('.dropdown').find('.menu').find('.item').removeClass('disabled');
    selectedValues.forEach(function(value) {
      $('#exclude_tags>option[value=' + value + ']').prop('disabled', true);
      $('#exclude_tags').closest('.dropdown').find('.menu').find('.item[data-value="' + value + '"]').addClass('disabled');
    });
  });
  $('#exclude_tags').on('change', function() {
    let selectedValues = $(this).val();
    $('#include_tags>option').prop('disabled', false);
    $('#include_tags').closest('.dropdown').find('.menu').find('.item').removeClass('disabled');
    selectedValues.forEach(function(value) {
      $('#include_tags>option[value=' + value + ']').prop('disabled', true);
      $('#include_tags').closest('.dropdown').find('.menu').find('.item[data-value="' + value + '"]').addClass('disabled');
    });
  });

  let helper = document.getElementById('filter_helper');
  let baseUrl = helper.dataset.filterSubmitUrl;

  let seriesFiltersForm = document.getElementById('series_filters_form');
  seriesFiltersForm.addEventListener('submit', function(event) {
    event.preventDefault();

    let formData = new FormData(this);
    let url = new URL(baseUrl);
    // noinspection JSCheckFunctionSignatures
    let params = new URLSearchParams(formData);

    for (let p of params) {
      //p[0]: key | p[1]: value
      //multiple select have one value per iteration
      if (p[1] === '') {
        params.delete(p[0]);
      }
    }

    //Delete inclusion_mode param to prevent url pollution
    if (!params.has('include_tags[]') && !params.has('exclude_tags[]')) {
      params.delete('inclusion_mode');
    }

    url.searchParams = params;
    url.search += '?' + decodeURIComponent(params.toString());

    location.href = url.toString().replace('/\t/g', '+');
  });
});