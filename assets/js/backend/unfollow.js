$(function () {

    $(".unfollow").on('click', function (evt) {
        evt.preventDefault();

        let unfollowUri = $(this).attr('href');
        let $series = $(this).closest('.card');

        $.ajax({
            url: unfollowUri,
            type: "DELETE",
            success: function (result) {
                if (result.success === true) {
                    $series.slideUp(400, function () {
                        $series.remove();
                    });
                }
            }
        });
    });
});