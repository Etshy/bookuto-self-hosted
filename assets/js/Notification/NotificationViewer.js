import NotificationDTO from './NotificationDTO';

export default class NotificationViewer {

  #chapterLabelTranslated;
  #renderUrl;
  #notificationUrl;
  #chapterUrl;
  #sseNotificationUrl;
  #latestsNotificationsUrl;
  #$templateElement = document.getElementById('notification_item');

  constructor(options) {
    this.#chapterLabelTranslated = options.chapterLabelTranslated;
    this.#renderUrl = options.renderUrl;
    this.#notificationUrl = options.notificationUrl;
    this.#chapterUrl = options.chapterUrl;
    this.#sseNotificationUrl = options.sseNotificationUrl;
    this.#latestsNotificationsUrl = options.latestsNotificationsUrl;

    //improvment : add a property "unread", instead of calculate it each time.
    // watch for changes of "unread" and call updateUnreadCounter when it changes

  }

  init() {

    this.#getLatestsNotifications().catch(() => {
      //Error getLatestsNotifications (most likely not connected)
    }).then(() => {
      setTimeout(() => this.#getNewNotifications(), 30000); // 30 seconds, no need to check  for new notif just after.
    });

  }

  /**
   *
   * @param {String} url
   * @param {object} data
   *
   * @return {Promise}
   */
  static #doXHRRequest(url, data = null) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onload = () => resolve(xhr.responseText);
      xhr.onerror = () => reject(xhr.statusText);
      xhr.send(data);
    });
  }

  static #onClickItem(e) {
    let Element = e.currentTarget;
    let url = Element.dataset.notificationurl;
    NotificationViewer.#doXHRRequest(url).then(() => {

      let unread = 0;
      let Notifications = document.getElementById('notifications_list').querySelectorAll(':scope>.item');
      Notifications.forEach(e => {
        if (e.classList && e.classList.contains('unread')) {
          unread++;
        }
      });

      //should have only one chilmd with ribbon class but ...
      let ribbonElements = Element.querySelectorAll('.ribbon');
      ribbonElements.forEach(e => e.remove());
      Element.classList.remove('unread');

      unread--;

      NotificationViewer.#updateUnreadCounter(unread);
    });
  }

  async #getLatestsNotifications() {
    let url = this.#latestsNotificationsUrl;
    //TOOD build parameters in url
    let response = await NotificationViewer.#doXHRRequest(url);
    /**
     * @type {{notifications:array, unreadCount:int}} response
     */
    response = JSON.parse(response);
    let notifications = response.notifications;
    let unread = 0;

    for (const responseKey in notifications) {
      const NotificationDto = new NotificationDTO(notifications[responseKey]);
      this.#createNotificationItem(NotificationDto);
      unread = (NotificationDto.readen) ? unread : unread + 1;
    }

    NotificationViewer.#updateUnreadCounter(unread);
  }

  static #updateUnreadCounter(unread) {
    if (unread > 0) {
      document.getElementById('unread_notification_nb').innerText = String(unread);
      document.getElementById('unread_notification_nb').classList.remove('hidden');
    } else {
      document.getElementById('unread_notification_nb').innerText = null;
      document.getElementById('unread_notification_nb').classList.add('hidden');
    }
  }

  #getNewNotifications() {
    let self = this;
    let source = new EventSource(this.#sseNotificationUrl);
    source.addEventListener('notifications', function(event) {
      let data = JSON.parse(event.data);
      if (data == null || data === false) {
        return false;
      }
      if (data.error) {
        return false;
      }

      let notifications = data;
      if (notifications.length > 0) {

        for (const responseKey in notifications) {
          let notificationDto = new NotificationDTO(notifications[responseKey]);
          self.#createNotificationItem(notificationDto);
        }
        // if unread notification, create a toast to inform the user ?
        NotificationViewer.#updateUnreadCounter(notifications.length)
      }
    }, false);

  }

  /**
   *
   * @param {NotificationDTO} notificationDto
   */
  #createNotificationItem(notificationDto) {

    let $cloneTemplate = document.importNode(this.#$templateElement.content, true);
    $cloneTemplate.id = '';
    $cloneTemplate.querySelector('#notification_title').innerHTML = notificationDto.chapter.series.name;
    $cloneTemplate.querySelector('.ui.item').id = 'cn_' + notificationDto.id;
    let chapterLabel;
    if (notificationDto.chapter.series.customTitle) {
      chapterLabel = notificationDto.chapter.series.customTitle;
    } else {
      chapterLabel = this.#chapterLabelTranslated;
    }
    let chapterText = chapterLabel + ' ' + notificationDto.chapter.number;
    if (notificationDto.chapter.name) {
      chapterText += ': ' + notificationDto.chapter.name;
    }
    $cloneTemplate.querySelector('#notification_description').innerHTML = chapterText;
    $cloneTemplate.querySelector('#notification_description').id = null;

    if (notificationDto.readen) {
      $cloneTemplate.querySelector('#notification_new_label').remove();
    } else {
      $cloneTemplate.querySelector('#notification_new_label').id = null;
      $cloneTemplate.querySelector('.ui.item').classList.add('unread');
    }

    if (notificationDto.chapter.series.image) {
      let url = this.#renderUrl;
      $cloneTemplate.querySelector('#notification_image').setAttribute('src', url.replace('placeholder', notificationDto.chapter.series.image));
    }
    $cloneTemplate.querySelector('#notification_image').id = null;

    let url = this.#notificationUrl;
    $cloneTemplate.querySelector('.item').setAttribute('data-notificationUrl', url.replace('placeholder', notificationDto.id));
    url = decodeURI(this.#chapterUrl);
    url = url.replace('{seriesSlug}', notificationDto.chapter.series.slug).replace('9999999999', notificationDto.chapter.number).replace('{language}', notificationDto.chapter.language);
    $cloneTemplate.querySelector('.item').setAttribute('href', url);

    document.getElementById('notifications_list').appendChild($cloneTemplate);

    const observer = new MutationObserver((mutations, obs) => {

      for (let mutationIndex in mutations) {
        let MutationRecord = mutations[mutationIndex];
        let notificationItem = MutationRecord.addedNodes.item(1); // for some reason seems like the Element is in key 1
        if (notificationItem) {
          notificationItem.addEventListener('click', NotificationViewer.#onClickItem);
        }
      }

      obs.disconnect();
    });

    observer.observe(document.getElementById('notifications_list'), {
      childList: true,
    });
  }
}