class NotificationDTO {
  /**
   * @typedef Series
   * @property {string} id
   * @property {string} name
   * @property {string} customTitle
   * @property {string} image
   */


  /**
   * @var {Object}
   */
  #user;
  /**
   * @type {boolean}
   */
  #readen;
  /**
   * @type {boolean}
   */
  #sent;
  /**
   * @type {string}
   */
  #type;
  /**
   * @type {Object}
   * @property {Series} series
   */
  #chapter;
  /**
   * @type {string}
   */
  #id;

  /**
   *
   * @param {{user:object, readen: boolean, sent:boolean, id:string, chapter:object, type:string}} data
   */
  constructor(data) {
    this.#user = data.user;
    this.#readen = data.readen;
    this.#user = data.user;
    this.#chapter = data.chapter;
    this.#sent = data.sent;
    this.#type = data.type;
    this.#id = data.id;
  }

  get id() {
    return this.#id;
  }

  get user() {
    return this.#user;
  }

  get readen() {
    return this.#readen;
  }

  get sent() {
    return this.#sent;
  }

  get type() {
    return this.#type;
  }

  get chapter() {
    return this.#chapter;
  }
}

export default NotificationDTO;