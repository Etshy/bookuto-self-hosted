$(function () {
    $(".delete_chapter").on("click", function (evt) {
        evt.preventDefault();

        let href = $(this).attr('href');
        let $chapterRow = $(this).closest('.item');

        $.ajax({
            url: href,
            type: "DELETE",
            success: function(result) {
                if (result.success === true) {
                    $chapterRow.slideUp(400, function () {
                        $(this).remove();
                    });
                }
            }
        });
    });
});